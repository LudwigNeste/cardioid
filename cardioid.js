let canvas;
let context;
let drawTime = 0;
let drawPrecision = 10;
let r;
let t = 3000;
let button;
let input;
let animate;
let drawStep = 2;
let textWidth;
function init() {
    canvas = document.getElementsByTagName("canvas")[0];
    canvas.width = document.body.clientWidth * 0.8;
    canvas.height = document.body.clientHeight * 0.8;
    document.body.appendChild(canvas);
    context = canvas.getContext("2d");
    context.moveTo(0, 0);
    context.fillStyle = "#FFFFFF";
    context.fillRect(0, 0, canvas.width, canvas.height);
    console.log("begin");
    context.fill();
    context.fillStyle = "#000000";
    context.strokeStyle = "#000000";
    context.font = "20px Arial";
    textWidth = context.measureText("888").width;
    if (canvas.width < canvas.height)
        r = canvas.width / 2.0 - textWidth;
    else
        r = canvas.height / 2.0 - textWidth;
    //draw numbers
    let test;
    button = document.getElementsByTagName("button")[0];
    input = document.getElementsByTagName("input")[0];
    input.value = (drawPrecision).toString();
    input.addEventListener("change", () => {
        drawPrecision = input.valueAsNumber;
        redrawNumbers();
    });
    let check = document.getElementsByTagName("input")[2];
    check.addEventListener("change", (event) => {
        animate = check.checked;
        draw();
    });
    let stepInput = document.getElementsByTagName("input")[1];
    stepInput.value = drawStep.toString();
    stepInput.addEventListener("change", () => {
        drawStep = stepInput.valueAsNumber;
    });
    button.onclick = draw;
    redrawNumbers();
}
function redrawNumbers() {
    drawTime = 0;
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.beginPath();
    input.value = drawPrecision.toString();
    for (let i = 0; i < drawPrecision; i++) {
        let textDim = context.measureText(i.toString());
        context.fillText((i).toString(), Math.floor(canvas.width / 2.0 + r * Math.cos(i / drawPrecision * 2 * Math.PI) - textDim.width / 2.0), Math.floor(canvas.height / 2.0 + r * Math.sin(i / drawPrecision * 2 * Math.PI) + 20 / 2.0));
        context.fill();
    }
}
function draw() {
    drawTime++;
    context.moveTo(Math.floor(canvas.width / 2.0 + r * Math.cos(drawTime / drawPrecision * 2 * Math.PI)), Math.floor(canvas.height / 2.0 + r * Math.sin(drawTime / drawPrecision * 2 * Math.PI)));
    context.lineTo(Math.floor(canvas.width / 2.0 + r * Math.cos(drawTime * drawStep / drawPrecision * 2 * Math.PI)), Math.floor(canvas.height / 2.0 + r * Math.sin(drawTime * drawStep / drawPrecision * 2 * Math.PI)));
    context.stroke();
    if (drawTime % drawPrecision == 0) {
        drawPrecision++;
        redrawNumbers();
    }
    if (animate)
        setTimeout(draw, 100);
}
init();
